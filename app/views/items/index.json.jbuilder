json.array!(@items) do |item|
  json.extract! item, :id, :name, :website, :city, :address, :category, :description, :logo
  json.url item_url(item, format: :json)
end
