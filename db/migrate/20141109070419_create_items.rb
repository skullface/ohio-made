class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :website
      t.string :city
      t.string :address
      t.string :category
      t.text :description
      t.string :logo

      t.timestamps
    end
  end
end
